-- Param 1 : The file to download
-- Param 2 : The new name for the file
local repoFile, localFile = ...
local repo = "https://gitlab.com/Jiogo18/cc-scripts/-/raw/main/"

if localFile == nil then
  localFile = repoFile
end

localFilePath = shell.resolve(localFile)

-- Remove the file if it already exists
if fs.exists(localFilePath) then
  fs.delete(localFilePath)
end

-- Download the file and save it in the current directory
shell.run("wget "..repo..repoFile.." "..localFile)
