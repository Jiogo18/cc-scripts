local prevTableConfig = _G.Config
local prevTableFood = _G.Food
_G.Config = nil
_G.Food = nil
require("food")
require("config")

-- Update the previous tables so we don't need to restart the program
if prevTableConfig then
  prevTableConfig.numberOfFlowers = Config.numberOfFlowers
  prevTableConfig.maxPreviousFoods = Config.maxPreviousFoods
  prevTableConfig.wrapInvIn = Config.wrapInvIn
  prevTableConfig.wrapInvOut = Config.wrapInvOut
  prevTableConfig.wrapScreenLog = Config.wrapScreenLog
  _G.updatePeripherals()
end
if prevTableFood then
  prevTableFood.hunger = Food.hunger
  prevTableFood.withHunger = Food.withHunger
end
