-- List of food items with their hunger values
-- Call food.withHunger(item) to set the hunger value of an item
-- The saturation is not stored here
if not _G.Food then
  _G.Food = {}
end
Food = _G.Food

if not Food.hunger then
  Food.hunger = {}
end
local h = Food.hunger
local hungerMax = 12
-- Hunger is an integer, you have to multiply the amount displayed by 2
-- 1 hunger = 0.5 food point
-- Bread: 5 hunger = 2.5 food points
h["minecraft:potato"] = 1
h["minecraft:baked_potato"] = 5
h["minecraft:bread"] = 5
h["minecraft:apple"] = 4
h["minecraft:melon_slice"] = 2

h["pamhc2crops:mustardseedsseeditem"] = 3
h["pamhc2crops:soybeanseeditem"] = 3
h["pamhc2foodcore:bakedvegetablemedlyitem"] = 7
h["pamhc2foodcore:butteritem"] = 6
h["pamhc2foodcore:fruitpunchitem"] = 13
h["pamhc2foodcore:jellydonutitem"] = 12
h["pamhc2foodcore:p8juiceitem"] = 7
h["pamhc2foodcore:stewitem"] = 15
h["pamhc2foodextended:bbqsauceitem"] = 5
h["pamhc2foodextended:cheeseontoastitem"] = 10
h["pamhc2foodextended:coffeeconlecheitem"] = 16
h["pamhc2foodextended:cornedbeefbreakfastitem"] = 44
h["pamhc2foodextended:cornedbeefhashitem"] = 29
h["pamhc2foodextended:cornedbeefitem"] = 29
h["pamhc2foodextended:currypowderitem"] = 16
h["pamhc2foodextended:fairybreaditem"] = 12
h["pamhc2foodextended:guacamoleitem"] = 13
h["pamhc2foodextended:honeysandwichitem"] = 14
h["pamhc2foodextended:hotdishcasseroleitem"] = 20
h["pamhc2foodextended:elderberryjellytoastitem"] = 10
h["pamhc2foodextended:strawberryjellytoastitem"] = 10
h["pamhc2foodextended:blackberryjellytoastitem"] = 10
h["pamhc2foodextended:elderberryjellysandwichitem"] = 12
h["pamhc2foodextended:strawberryjellysandwichitem"] = 12
h["pamhc2foodextended:blackberryjellysandwichitem"] = 12
h["pamhc2foodextended:grapeberryjellysandwichitem"] = 12
h["pamhc2foodextended:grapejellysandwichitem"] = 12

function Food:withHunger(item)
  if not item then return 0 end
  local name = item.name
  if not Food.hunger[name] then
    print("Warning: hunger value of " .. name .. " not found")
    Food.hunger[name] = 0
  end
  item.hunger = math.min(hungerMax, Food.hunger[name])
  return item.hunger
end
