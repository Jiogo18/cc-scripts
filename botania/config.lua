-- Configs
if not _G.Config then
  _G.Config = {}
end
Config = _G.Config

-- Modem names
Config.wrapInvIn = "minecraft:chest_0"
Config.wrapInvOut = "minecraft:chest_1"
Config.wrapScreenLog = "monitor_1"
Config.redstonePulseSide = "back"

-- Number of items to send at once
Config.numberOfFlowers = 4

-- Number of different foods eaten in a row gives a boost (8 items max = +80%)
-- nb#   1  2  3    4    5    6    7     8
-- boost 0  1  1.3  1.5  1.6  1.7  1.75  1.8 
Config.maxPreviousFoods = 8

-- Additionnal time to wait between cycles (in seconds)
-- This is a margin to avoid sending items too fast
Config.timeMarginBetweenCycles = 1

-- Time to wait if no food found (in seconds)
Config.timeWaitIfNoFoodFound = 60

Config.terminateIfOutputNotEmpty = true