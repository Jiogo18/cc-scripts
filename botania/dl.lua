-- Check if the current directory is botania/
local currentDir = shell.dir()
if string.sub(currentDir, -7) ~= "botania" then
  print("Please run this script from the botania/ directory")
  return
end

local repo = "https://gitlab.com/Jiogo18/cc-scripts/-/raw/main/"

-- Files to download
local files = {
  "test_move_items.lua",
  -- "dl.lua"
  -- "dlrun.lua"
}

-- Download each file
for i = 1, #files do
  local filePath = shell.resolve(files[i])
  if fs.exists(filePath) then
    fs.delete(filePath)
  end
  shell.run("wget "..repo.."botania/"..files[i])
end
