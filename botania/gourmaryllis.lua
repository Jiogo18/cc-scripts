

-- Init
require("food")
require("config")

local invIn = nil
local invOut = nil
local monitorLog = nil

function _G:updatePeripherals()
  invIn = peripheral.wrap(Config.wrapInvIn)
  invOut = peripheral.wrap(Config.wrapInvOut)
  monitorLog = peripheral.wrap(Config.wrapScreenLog)
  local isOk = true
  if not invIn then
    print("Error: Could not find inventory " .. Config.wrapInvIn)
    isOk = false
  end
  if not invOut then
    print("Error: Could not find inventory " .. Config.wrapInvOut)
    isOk = false
  end
  if not monitorLog then
    print("Error: Could not find monitor " .. Config.wrapScreenLog)
    isOk = false
  end
  if not isOk then shell.exit() end
end
_G:updatePeripherals()

_G.previousFoods = {}
local previousFoods = _G.previousFoods
local noFoodFoundInARow = 0
_G.minTimeBetweenCycle = 0

-- Functions
function isPreviousFood(itemName)
  for i = 1, #previousFoods do
    if previousFoods[i] == itemName then
      return true
    end
  end
  return false
end

function betterFood(previous, item)
  if not item or item.count < Config.numberOfFlowers then return false end
  if isPreviousFood(item.name) then return false end
  Food:withHunger(item)
  if not item.hunger or item.hunger <= 0 then return false end
  if previous ~= nil and item.hunger <= previous.hunger then return false end
  return true
end

function findBestFoodInChest(chest)
  local bestFood = nil
  for slot, item in pairs(chest.list()) do
    if betterFood(bestFood, item) then
      item.slot = slot
      bestFood = item
    end
  end
  return bestFood
end

function moveFood(invFrom, invTo, food)
  -- Check if invTo has items
  if #invTo.list() > 0 and Config.terminateIfOutputNotEmpty then
    print("Error: " .. Config.wrapInvOut .. " is not empty")
    shell.exit()
  end
  -- Move food to invTo
  local slot = invFrom.pushItems(peripheral.getName(invTo), food.slot, Config.numberOfFlowers)
  if not slot then
    print("Error: Could not move food")
    return
  end
  -- Add food to previousFoods
  table.insert(previousFoods, food.name)
  while #previousFoods > Config.maxPreviousFoods-1 do
    table.remove(previousFoods, 1)
  end
end

function getCurrentTime()
  local time = os.time("local")
  local hours = math.floor(time)
  local minutes = math.floor((time - hours) * 60)
  local seconds = math.floor(((time - hours) * 60 - minutes) * 60)
  if hours < 10 then hours = "0" .. hours end
  if minutes < 10 then minutes = "0" .. minutes end
  if seconds < 10 then seconds = "0" .. seconds end
  return hours .. ":" .. minutes .. ":" .. seconds
end

function log(text)
  print("[" .. getCurrentTime() .. "] " .. text)
end

-- find a food, move it to invOut and return its hunger value
function nextFood()
  if invIn == nil or invOut == nil then
    _G:updatePeripherals()
    if invIn == nil then
      print("Error: Could not find input inventory " .. Config.wrapInvIn)
      shell.exit()
    elseif invOut == nil then
      print("Error: Could not find output inventory " .. Config.wrapInvOut)
      shell.exit()
    end
  end

  local item = findBestFoodInChest(invIn, previousFoods)
  if item then
    if noFoodFoundInARow > 0 then
      print() -- New line
      noFoodFoundInARow = 0
    end
    log("Next food: \u{4}" .. item.hunger .. " " .. item.name)
    moveFood(invIn, invOut, item)
    return item.hunger
  else
    noFoodFoundInARow = noFoodFoundInARow + 1
    if noFoodFoundInARow == 1 then
      log("No food found")
    else
      write(".")
    end
    return 0
  end
end

-- send redstone pulse to the dropper in a 4 gt loop
function sendPulse()
  if not Config.redstonePulseSide then return 0 end
  for i = 1, Config.numberOfFlowers do
    redstone.setOutput(Config.redstonePulseSide, true)
    sleep(0.1)
    redstone.setOutput(Config.redstonePulseSide, false)
    sleep(0.1)
  end
  return Config.numberOfFlowers * 0.2
end

-- Main
log("Started")
term.redirect(monitorLog)
print("=== Gourmaryllis ===")
log("Started")
while true do
  local hunger = nextFood()
  if hunger > 0 then
    -- 2 hunger = 1 food point = 1 second
    time = hunger * 0.5

    -- Send redstone pulse
    time = time - sendPulse()

    if time < Config.timeMarginBetweenCycles then
      print("Error: Time is too low, either increase timeMarginBetweenCycles or remove the food with less hunger")
      print("Time: " .. time)
      print("timeMarginBetweenCycles: " .. Config.timeMarginBetweenCycles)
      shell.exit()
    elseif time < 0 then
      print("Error: Time is negative, you have too many flowers or the food doesn't give enough hunger")
      print("Time: " .. time)
      shell.exit()
    end
    sleep(time + 0.1)
  else
    sleep(Config.timeWaitIfNoFoodFound)
  end
end
